# OpenML dataset: CNS

https://www.openml.org/d/45086

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Central Nervous System (CNS, Embrional-T) dataset**

**Authors**: S. Pomeroy, P. Tamayo, M. Gaasenbeek, L. Sturla, M. Angelo, M. McLaughlin, J. Kim, L. Goumnerova, P. Black, C. Lau, et al

**Please cite**: ([URL](https://www.scopus.com/record/display.uri?eid=2-s2.0-0037165140&origin=inward)): S. Pomeroy, P. Tamayo, M. Gaasenbeek, L. Sturla, M. Angelo, M. McLaughlin, J. Kim, L. Goumnerova, P. Black, C. Lau, et al, Prediction of central nervous system embryonal tumour outcome based on gene expression, Nature 415 (6870) (2002) 436-442.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45086) of an [OpenML dataset](https://www.openml.org/d/45086). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45086/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45086/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45086/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

